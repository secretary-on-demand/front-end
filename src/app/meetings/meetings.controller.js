// controller for <meetings> directive
angular.module('app.meetings')
    .controller('meetingsCtrl', meetingsCtrl);

function meetingsCtrl($scope, $location, meeting) {
    var ctrl = this;

    meeting.addObserver(function(meetingData) {
        $scope.meetings = meeting.getMeetings();
    });

    $scope.meetings = meeting.getMeetings();

    $scope.loadMeeting = function(id) {
        $location.path('/meeting/').search({'id': id});
        meeting.setCurrentMeeting(id);
    }

    $scope.createMeeting = function() {
        meeting.setCurrentMeeting(-1);
    }

}
