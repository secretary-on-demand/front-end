// define <meetings></meetings> directive
angular.module('app.meetings')
    .directive('dMeetings', meetingsDirective);

function meetingsDirective() {
    return {
        scope: {},
        templateUrl: 'meetings/meetings.html',
        replace: true,
        controller: 'meetingsCtrl',
        controllerAs: 'ctrl',
        bindToController: true
    };
}