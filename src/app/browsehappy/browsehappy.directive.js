// define <browsehappy></browsehappy> directive
angular.module('app.browsehappy')
    .directive('browsehappy', browsehappyDirective);

function browsehappyDirective() {
    return {
        restrict: 'E',
        templateUrl: 'browsehappy/browsehappy.html',
        replace: true
    };
}