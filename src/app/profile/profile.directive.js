// define <profile></profile> directive
angular.module('app.profile')
    .directive('dProfile', profileDirective);

function profileDirective() {
    return {
        scope: {},
        templateUrl: 'profile/profile.html',
        replace: true,
        controller: 'profileCtrl',
        controllerAs: 'ctrl',
        bindToController: true
    };
}