angular.module('app.core')
    .factory('meeting', ['storage', meetingService]);

function meetingService(storage) {
    var service = {
        'getMeetings': getMeetings,
        'setMeeting': setMeeting,
        'getMeeting': getMeeting,
        'removeMeeting': removeMeeting,
        'addObserver': addObserver,
        'setCurrentMeeting': notifyAll
    };

    service.observers = [];

    function addObserver(observer) {
        service.observers.push(observer);
    }

    function notifyAll(id) {
        angular.forEach(service.observers, function(observer) {
            observer(getMeeting(id));
        });
    }

    function getMeetings() {
        return storage.getList('meetings');
    }

    function getMeeting(id) {
        if (id == -1) {
            return {};
        }
        return storage.getFromList('meetings', 'id', id);
    }

    function setMeeting(meeting) {
        if (meeting.id == undefined) {
            if (meeting.title != "") {
                var meetings = getMeetings();
                var id = -1;
                for (var index in meetings) {
                    id = Math.max(id, meetings[index].id);
                }
                meeting.id = id + 1;
                storage.addToList('meetings', meeting);
            }
        } else {
            removeMeeting(meeting);
            storage.addToList('meetings', meeting);
        }
        return meeting;
    }

    function removeMeeting(meeting) {
        storage.removeFromList('meetings', 'id', meeting.id);
    }

    return service;

};