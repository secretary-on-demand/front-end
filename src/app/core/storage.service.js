angular.module('app.core')
    .factory('storage', storageService);

function storageService() {
    var service = {
        'put': put,
        'get': get,
        'addToList': addToList,
        'getList': getList,
        'removeFromList': removeFromList,
        'getFromList': getFromList
    };

    function put(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    }

    function get(key) {
        return JSON.parse(localStorage.getItem(key));
    }

    function getList(listName) {
        var result = get(listName);
        if (result == null) {
            return [];
        } else {
            return result;
        }
    }

    function setList(listName, listData) {
        put(listName, listData);
    }

    function addToList(listName, item) {
        var currentList = getList(listName);
        currentList.push(item);
        put(listName, currentList);
    }

    function removeFromList(listName, attributeName, attributeValue) {
        var newList = [];
        var list = getList(listName);
        for (var index in list) {
            var item = list[index];
            if (item[attributeName] != attributeValue) {
                newList.push(item);
            }
        }
        setList(listName, newList);
    }

    function getFromList(listName, attributeName, attributeValue) {
        var list = getList(listName);
        for (var index in list) {
            var item = list[index];
            if (item[attributeName] == attributeValue) {
                return item;
            }
        }
        return null;
    }

    return service;
};