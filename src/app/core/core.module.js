// all application specific modules depend on this module
// this core module defines the shared services and 3th party dependencies
angular.module('app.core', [
    'ui.router'
]);