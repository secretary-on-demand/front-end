// Register the directive
angular.module('app.meeting')
    .directive('dMeeting', meetingDirective);

/**
 * The directive.
 */
function meetingDirective() {
    return {
        scope: {},
        templateUrl: 'meeting/meeting.html',
        replace: true,
        controller: 'meetingCtrl',
        controllerAs: 'ctrl',
        bindToController: true
    };
}