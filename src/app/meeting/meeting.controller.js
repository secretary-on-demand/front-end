// Register the controller
angular.module('app.meeting')
    .controller('meetingCtrl', meetingCtrl);

/**
 * Controller for the directive.
 */
function meetingCtrl($scope, $location, $filter, meeting) {
    var ctrl = this;

    meeting.addObserver(function(meetingData) {
        loadMeeting(meetingData);
    });

    function loadMeeting(meetingData) {
        // Load new meeting
        if (meetingData != null && meetingData.id != undefined && meetingData.id != $scope.meeting.id) {
            // Save old meeting (if there is any)
            if ($scope.meeting != []) {
                meeting.setMeeting($scope.meeting);
            }
            $scope.meeting = meetingData;
            $scope.state = 'closed';
            $scope.action = 'Continue meeting';
            if (meetingData.id == undefined) {
                $scope.state = 'initial';
                $scope.action = 'Start meeting';
            }
        }
        if (meetingData == null || meetingData.id == undefined) {
            $scope.state = 'initial';
            $scope.action = 'Start meeting';
            $scope.meeting = createNewMeeting();
        }
    }

    function save() {
        if ($scope.state != 'initial' && meeting != null) {
            $scope.meeting = meeting.setMeeting($scope.meeting);
            meeting.setCurrentMeeting($scope.meeting.id);
        }
    }

    /**
     * Initialize the meeting.
     */
    function init() {
        // The state and state labels
        $scope.state = 'initial';
        $scope.action = 'Start meeting';

        // The meeting
        $scope.meeting = createNewMeeting();

        // Date format
        $scope.dateFormat = 'yyyy-MM-dd HH:mm';

        // Fetch the ID from the query parameters
        var id = $location.search().id;
        if (id != undefined) {
            loadMeeting(meeting.getMeeting(id));
        }
    }

    function createNewMeeting() {
        return {
            'startedAt': null,
            'closedAt': null,
            'title': "",
            'recognized': ""
        }
    }

    /**
     * Capitalize the first letter of a string.
     *
     * @param string string to convert
     * @returns {string} string from which the first letter is capitalized
     */
    function capitaliseFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    /**
     * Speech-to-text functionality.
     *
     * @todo replace by a more accurate algorithm
     */
    function record() {
        var recognition = new webkitSpeechRecognition();
        recognition.continuous = true;
        recognition.interimResults = true;

        // Handle voice recognition events
        recognition.onresult = function(event) {

            // Fetch the last results
            var result = event.results[event.results.length - 1];
            var text = result[0].transcript;

            // Evaluate the result when it is final (e.g. the voice recognition has found a final version
            // of the speech-to-text conversion)
            if (result.isFinal == true) {
                text = $.trim(text);
                $scope.meeting.recognized += capitaliseFirstLetter(text) + '. ';
                save();
                $scope.$apply();
            }

        }

        recognition.start();
    }

    $scope.save = function() {
        save();
    }

    $scope.remove = function() {
        meeting.removeMeeting($scope.meeting);
        $scope.meeting = createNewMeeting();
        meeting.setCurrentMeeting(-1);
    }

    /**
     * Toggle the action triggered by the action button.
     */
    $scope.toggleAction = function() {
        if ($scope.state == 'initial') {
            // Actions at the initial state
            $scope.meeting.startedAt = new Date();
            if ($scope.meeting.title == "") {
                $scope.meeting.title = $filter('date')($scope.meeting.startedAt, $scope.dateFormat);
            }
        }

        if ($scope.state == 'initial' || $scope.state == 'closed') {
            // initial -> recording, closed -> recording
            $scope.state = 'recording';
            $scope.action = 'Close meeting';
            record();
        } else {
            // recording -> closed
            $scope.meeting.closedAt = new Date();
            $scope.state = 'closed';
            $scope.action = 'Continue meeting';
        }

        save();
    }

    // Initialize
    init();

}
