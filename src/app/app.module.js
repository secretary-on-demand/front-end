'use strict';

angular.module('app', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',

    'application',

    'app.core',
    'app.browsehappy',
    'app.meetings',
    'app.meeting',
    'app.profile'
]);
